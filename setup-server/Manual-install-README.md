# IoTtalk安裝指南

環境部分：(optional)
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install vim
sudo apt-get install ssh
sudo /etc/init.d/ssh  restart

========================================================
IoTtalk installation：

1. Download Python 3.6.5 into the user home directory, for example: (now the current working directory is "/home/<userID>")
        wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
   
2. Upload screen into the user home directory, then use the command below: (now the current working directory is "/home/<userID>")
        wget http://pcs.csie.nctu.edu.tw/da/screenrc
        mv screenrc .screenrc
    

3. Compile Python3.6.5 source file: (now the current working directory is "/home/<userID>")
	sudo -H apt-get install libsqlite3-dev -y
	sudo -H apt-get install libssl-dev openssl  -y
        sudo -H apt-get install zlib1g-dev -y
	tar xzvf Python-3.6.5.tgz
	cd Python-3.6.5         (now the current working directory is "/home/<userID>/Python-3.6.5")

        sudo -H apt-get install build-essential -y  (if you don't have development kits like c, c++ compiler)

	./configure --prefix=/usr/local
	make -j 2  
	sudo make install
        cd ..

3. Install screen
	sudo apt-get install screen -y
        

4. Install IoTtalk (now the current working directory is "/home/<userID>")
        sudo apt-get install git -y  (if you don't have git)
	git clone https://gitlab.com/IoTtalk/iottalk-v1.git iottalk_server_1.0    (then enter your GitLab account/password)
	cd iottalk_server_1.0      (now the current working directory is  "/home/<userID>/iottalk_server_1.0")
	sudo pip3 install -r requirements.txt

5. Run sudo command without a password
        cd /etc/sudoers.d  
        sudo touch nopasswd4sudo  
        sudo vim nopasswd4sudo  
   press 'i'
   Type 
        <userID> ALL=(ALL) NOPASSWD : ALL  
   press ESC, then :wq!  

6. Autorun when system reboot:
        sudo vim /etc/rc.local
   add a new line with the below command:
        sudo -u <userID> /home/<userID>/iottalk_server_1.0/setup/startup.sh

7. sudo apt-get install mailutils -y

8. sudo reboot

9. Check the IoTtalk running correct or not with the below command:
        screen -ls
   press Cttl + left/right arrow keys can shift the terminals, then check there is no error message.
========================================================





用意為指定特定使用者執行，否則會在到root下執行，user登入後看不到

7. 重開機後，下指令 screen -ls，有沒有看到screen再跑，
   然後再下指令 screen -r，去看裡面有沒有五個分頁 
   (按 ctrl + 方向鍵會切換分頁)，若以上都有看到，
   且分頁內最後一行不是錯誤訊息，則表示安裝成功。

8. 啟動送mail功能 (Cyber device: Message的功能需要)
    sudo apt-get install mailutils






附錄：
安裝python3.7出現ModuleNotFoundError: No module named ‘_ctypes’解決辦法
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get dist-upgrade
    sudo apt-get install build-essential python-dev python-setuptools python-pip python-smbus
    sudo apt-get install build-essential libncursesw5-dev libgdbm-dev libc6-dev
    sudo apt-get install zlib1g-dev libsqlite3-dev tk-dev
    sudo apt-get install libssl-dev openssl
    sudo apt-get install libffi-dev
