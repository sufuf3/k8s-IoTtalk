#!/usr/bin/env bash
set -e -x -u
echo "=== Setup Environment ==="

# Install IoTtalk server dependents
sudo apt-get update
sudo apt-get upgrade -y
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -qq -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" \
git vim ssh tmux screen python3 python3-pip libsqlite3-dev libssl-dev openssl nodejs npm
sudo debconf-set-selections <<< "postfix postfix/mailname string iottalklab.nctu.edu.tw"
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
sudo apt-get install -qq -y mailutils
sudo -H pip3 install --upgrade pip
sudo -H pip3 install setuptools

# Add iottalk user
sudo useradd -m iottalk -s /bin/bash
sudo adduser iottalk sudo
sudo adduser iottalk adm
sudo cp -pr /home/vagrant/.ssh /home/iottalk/
sudo chown -R iottalk:iottalk /home/iottalk
echo '%iottalk ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers.d/iottalk

# Setup IoTtalk server backend
cd /home/iottalk/ && sudo -u iottalk git clone https://github.com/sufuf3/k8s-IoTtalk.git iottalk_server_1.0
cd /home/iottalk/iottalk_server_1.0 && sudo -u iottalk git checkout v1.0.0
cd /home/iottalk/iottalk_server_1.0 && sudo -u iottalk -H pip3 install -r requirements.txt
cd /home/iottalk/iottalk_server_1.0 && sudo -u iottalk -H pip3 install --upgrade requests

# Install uwsgi
sudo -H apt install libpcre3 libpcre3-dev -y
sudo -H pip3 install uwsgi

# Install nginx
sudo apt-get install -y nginx
sudo wget https://raw.githubusercontent.com/sufuf3/k8s-IoTtalk/master/setup-server/nginx-config/default -O /etc/nginx/sites-available/default
#sudo nginx -s reload
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update
sudo apt-get install -y python-certbot-nginx
sudo systemctl stop nginx.service
sudo systemctl disable nginx.service

# Setup autorun iottalk daemon
sudo wget https://raw.githubusercontent.com/sufuf3/k8s-IoTtalk/master/setup-server/systemctl/iottalk.service -O /etc/systemd/system/iottalk.service
sudo systemctl enable iottalk
sudo systemctl start iottalk
