# Setup IoTtalk with Kubernetes

[![](https://img.shields.io/docker/build/jrottenberg/ffmpeg.svg)](https://cloud.docker.com/repository/registry-1.docker.io/sufuf3/iottalk)

This project provides the methods of IoTtalk server setup. Include 4 methods: **`Setup IoTtalk Server on a host`**, **`Setup IoTtalk Container Server on a host`**, **`Setup IoTtalk Server with K8s on a host`**, **`Setup IoTtalk Server on mutiple hosts`**

## Table of Contents
- [Method 1: Setup IoTtalk Server on a host](#method-1-setup-iottalk-server-on-a-host)
  * [First Way: Using Vagrant](#first-way-using-vagrant)
    + [0. Requirements](#0-requirements)
    + [1. Setup](#1-setup)
  * [Second Way: Using shell script](#second-way-using-shell-script)
- [Method 2: Setup IoTtalk Container Server on a host](#method-2-setup-iottalk-container-server-on-a-host)
- [Method 3: Setup IoTtalk Server with K8s on a host](#method-3-setup-iottalk-server-with-k8s-on-a-host)
  * [First Way: Using Vagrant](#first-way-using-vagrant-1)
    + [0. Requirements](#0-requirements-1)
    + [1. Setup](#1-setup-1)
  * [Second Way: deploy by yourself](#second-way-deploy-by-yourself)
    + [Step 1. Setup K8s env](#step-1-setup-k8s-env)
    + [Step 2. Deploy IoTtalk Server](#step-2-deploy-iottalk-server)
      - [A. Normal deploy](#a-normal-deploy)
      - [B. Using Helm](#b-using-helm)
- [Method 4: Setup IoTtalk Server on mutiple hosts](#method-4-setup-iottalk-server-on-mutiple-hosts)
  * [Step 1. Setup K8s env](#step-1-setup-k8s-env-1)
  * [Step 2. Deploy IoTtalk Server](#step-2-deploy-iottalk-server-1)
    + [A. Normal deploy](#a-normal-deploy-1)
    + [B. Using Helm](#b-using-helm-1)
- [Usage](#usage)
- [Additional: Install VirtualBox & Vagrant](#install-virtualbox--vagrant)
  * [Install VirtualBox v5.1](#install-virtualbox-v51)
  * [Install vagrant](#install-vagrant)
- [Additional: Todo list](#todo-list)

## Method 1: Setup IoTtalk Server on a host

### First Way: Using Vagrant
Setup IoTtalk server on a VM.
#### 0. Requirements

Please refer to [Install VirtualBox & Vagrant](#install-virtualbox--vagrant)

#### 1. Setup

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ cd ~/k8s-IoTtalk/setup-server/ && vagrant up
```

### Second Way: Using shell script
Setup IoTtalk server on a host using script.

```sh
$ curl -Lo- http://bit.ly/2C1Yjeq | bash
```
## Method 2: Setup IoTtalk Container Server on a host
Setup IoTtalk container server on a host.

```sh
$ docker run -it -p 9999:9999 -p 7788:7788 -p 80:80 -p 5566:5566 -p 8000:8000 sufuf3/iottalk:version-1.0.1 -rm /bin/sh
/usr/lib/python2.7/dist-packages/supervisor/options.py:297: UserWarning: Supervisord is running as root and it is searching for its configuration file in default locations (including its current working directory); you probably want to specify a "-c" argument specifying an absolute path to a configuration file for improved security.
  'Supervisord is running as root and it is searching '
2019-01-05 09:22:56,996 CRIT Supervisor running as root (no user in config file)
2019-01-05 09:22:56,997 WARN Included extra file "/etc/supervisor/conf.d/supervisord.conf" during parsing
2019-01-05 09:22:57,007 INFO RPC interface 'supervisor' initialized
2019-01-05 09:22:57,007 CRIT Server 'unix_http_server' running without any HTTP authentication checking
2019-01-05 09:22:57,007 INFO supervisord started with pid 8
2019-01-05 09:22:58,010 INFO spawned: 'iottalk' with pid 11
2019-01-05 09:22:59,020 INFO success: iottalk entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
2019-01-05 09:23:06,305 INFO exited: iottalk (exit status 0; expected)
```

You can use another session to get container ID and exec into the container.

```sh
$ docker ps
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                                                          NAMES
afc1d4d86aef        sufuf3/iottalk:v1.0.1   "sh -c /usr/bin/supe…"   51 seconds ago      Up 51 seconds       80/tcp, 5566/tcp, 7788/tcp, 8000/tcp, 0.0.0.0:9999->9999/tcp   dreamy_leakey

$ docker exec -it afc1d4d86aef /bin/sh
# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0   4504   704 pts/0    Ss+  09:22   0:00 sh -c /usr/bin/supervisord -rm /bin/sh
root         8  0.1  0.1  47984 18388 pts/0    S+   09:22   0:00 /usr/bin/python2 /usr/bin/supervisord
root        38  0.0  0.0  25084  3568 ?        Ss   09:22   0:00 SCREEN -dmS iottalk_server_1.0
root        43  0.1  0.0   4504   752 pts/1    Ss+  09:22   0:00 /bin/sh
root        46  0.1  0.0   9568  2672 pts/2    Ss+  09:22   0:00 bash -c         while [ 1 ]; do             ./bin/csm python3; echo ==========
root        47  0.0  0.0   4504   792 pts/2    S+   09:22   0:00 /bin/sh ./bin/csm python3
root        48  0.5  0.2 175408 41344 pts/2    Sl+  09:22   0:00 python3 -m csm
root        51  1.6  0.2 333864 42668 pts/2    Sl+  09:22   0:01 /usr/bin/python3 /home/iottalk/iottalk_server_1.0/lib/csm.py
root        58  0.1  0.0   9568  2644 pts/3    Ss+  09:23   0:00 bash -c         while [ 1 ]; do             ./bin/ecsim python3; echo ========
root        61  0.1  0.0   9568  2572 pts/4    Ss+  09:23   0:00 bash -c         while [ 1 ]; do             ./bin/ccm python3; echo ==========
root        62  0.0  0.0   4504   736 pts/3    S+   09:23   0:00 /bin/sh ./bin/ecsim python3
root        63  0.7  0.2  88544 34156 pts/3    S+   09:23   0:00 python3 -m ecsim.ecsim
root        64  0.0  0.0   4504   736 pts/4    S+   09:23   0:00 /bin/sh ./bin/ccm python3
root        65  0.7  0.2 102416 41480 pts/4    S+   09:23   0:00 python3 -m ccm.ccm
root        67  0.1  0.0   9568  2648 pts/5    Ss+  09:23   0:00 bash -c         while [ 1 ]; do             ./bin/esm python3; echo ==========
root        69  0.0  0.0   4504   844 pts/5    S+   09:23   0:00 /bin/sh ./bin/esm python3
root        70  0.8  0.2  91052 34372 pts/5    S+   09:23   0:00 python3 -m esm
root        72  0.1  0.0   9572  2648 pts/6    Ss+  09:23   0:00 bash -c         while [ 1 ]; do             sudo python3 ./da/web.py; echo ===
root        75  0.0  0.0  46400  3424 pts/6    S+   09:23   0:00 sudo python3 ./da/web.py
root        76  0.2  0.1  75244 25336 pts/6    S+   09:23   0:00 python3 ./da/web.py
root        80  0.7  0.1 150936 25504 pts/6    Sl+  09:23   0:00 /usr/bin/python3 ./da/web.py
root        82  1.7  0.2 185964 43136 pts/4    Sl+  09:23   0:00 /usr/bin/python3 /home/iottalk/iottalk_server_1.0/lib/ccm/ccm.py
root        96  0.5  0.0   4504   692 pts/7    Ss   09:23   0:00 /bin/sh
root       103  0.0  0.0  34424  2796 pts/7    R+   09:23   0:00 ps aux
```

## Method 3: Setup IoTtalk Server with K8s on a host
Using kubeadm to setup K8s & deploy IoTtalk Server.

### First Way: Using Vagrant
Setup K8s & IoTtalk Server on a VM.

#### 0. Requirements

Please refer to [Install VirtualBox & Vagrant](#install-virtualbox--vagrant)

#### 1. Setup

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ cd ~/k8s-IoTtalk/setup-k8s-single-node/ && vagrant up
```

### Second Way: deploy by yourself

Setup K8s + IoTtalk Server on a host.

#### Step 1. Setup K8s env via kubeadm

Please refer to https://kubernetes.io/docs/setup/independent/install-kubeadm/

#### Step 2. Deploy IoTtalk Server

Choose A or B.

##### A. Normal deploy
```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ kubectl create -f ~/k8s-IoTtalk/deploy/iottalk-server/
```

##### B. Using Helm

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ helm install --name iottalk --debug ~/k8s-IoTtalk/deploy/helm/apps/iottalk-server/
```

## Method 4: Setup IoTtalk Server on mutiple hosts

Using kubespray to setup K8s & deploy IoTtalk Server.

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ cd ~/k8s-IoTtalk/setup-k8s-multi-node/
```

### Step 1. Setup K8s env
Setup K8s using [kubespray](https://github.com/kubernetes-sigs/kubespray).

### Step 2. Deploy IoTtalk Server

Choose A or B.

#### A. Normal deploy

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ kubectl create -f ~/k8s-IoTtalk/deploy/iottalk-server/
```

#### B. Using Helm

```sh
$ cd ~/ && git clone https://github.com/sufuf3/k8s-IoTtalk.git
$ helm install --name iottalk --debug ~/k8s-IoTtalk/deploy/helm/apps/iottalk-server/
```

---

## Usage
#### Access web

If you use method 1 & 2, please access to **`http://127.0.0.1:9999` & `http://127.0.0.1:7788/dfm`**; else, please access to **`http://127.0.0.1:31999` & `http://140.113.194.241:31788/dfm`**....

---

## Install VirtualBox & Vagrant

### Install VirtualBox v5.1

- Ubuntu 16.04
```sh
sudo bash -c 'echo "deb https://download.virtualbox.org/virtualbox/debian xenial contrib" >> /etc/apt/sources.list'
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install virtualbox-5.1
```

- Ubuntu 18.04
```
sudo bash -c 'echo "deb https://download.virtualbox.org/virtualbox/debian bionic contrib" >> /etc/apt/sources.list'
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install virtualbox-5.1
```

### Install vagrant

```sh
wget https://releases.hashicorp.com/vagrant/2.1.1/vagrant_2.1.1_x86_64.deb
sudo dpkg -i vagrant_2.1.1_x86_64.deb
sudo ln -s /usr/bin/vagrant /usr/local/bin/vagrant
vagrant version
```

---

## Todo List

### Todolist
- [x] ✅ Setup IoTtalk Server on a VM using vagrant
- [x] ✅ Setup on a host using shell sceipt
- [x] ✅ Write Dockerfile of IoTtalk Server
- [x] ✅ Setup Kubeadm+IoTtalk Server on a VM using vagrant
- [x] ✅ Setup Kubeadm+IoTtalk Server on a host
- [x] ✅ IoTtalk deployment & NodePort service yaml files
- [x] ✅ IoTtalk helm files
- [x] ✅ Kubespray + IoTtalk Setup README
- [ ] 🚧 IoTtalk statefulset yaml files
- [ ] 🚧 IoTtalk helm update to statefulset
- [ ] 🚧 sqlite problem

PS. If necessary, I'll create a installer project for IoTtalk Server with Kubespray.
